import React from 'react';
import Login from './src/pages/Login';
import HomeScreen from './src/pages/HomeScreen'

import {NavigationContainer} from '@react-navigation/native';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {createStackNavigator} from '@react-navigation/stack';
import AddReunion from './src/pages/AddReunion';
import ShowReunion from './src/pages/ShowReunion';

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="HomeScreen"  component={HomeScreen}/>
      <Stack.Screen name="AddReunion" component={AddReunion} />
      <Stack.Screen name="ShowReunion" component={ShowReunion} />
    </Stack.Navigator> 
    </NavigationContainer>
  );
}

