import {createStackNavigator} from 'react-navigation-stack';
import {NavigationContainer} from '@react-navigation/native';
import Login from '../pages/Login';
import HomeScreen from '../pages/HomeScreen';
import AddReunion from '../pages/AddReunion'; 
import ShowReunion from '../pages/ShowReunion';
import { createAppContainer } from 'react-navigation';

const screens = {
    Login: {
        screen: Login
    },
    HomeScreen: {
        screen: HomeScreen
    },
    AddReunion: {
        screen: AddReunion
    },
    ShowReunion: {
        screen: ShowReunion
    }
}

const HomeStack = createStackNavigator(screens);

export default createAppContainer(HomeStack)