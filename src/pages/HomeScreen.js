import { View, Text, BackHandler, StyleSheet, Button, ImageBackground } from "react-native";
import React from 'react'
import {Constants} from 'expo'
import FlatButton from '../components/button'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';


class HomeScreen extends React.Component {
    constructor(props){
        super(props);
        this.image = require('../resources/background2.jpg');
    }

    static navigationOptions={
        title:'HomeScreen',
        headerLeft:null
    }

    componentDidMount(){
        BackHandler.addEventListener('hardwareBackPress', function(){
            return false;
        });
    }

    addReunion(){
        this.props.navigation.navigate("AddReunion");
    }

    showReunions(){
        this.props.navigation.navigate("ShowReunion");
    }

    render(){
        return (
            <View style={styles.content}>
                <View style={styles.container}>
                    <View style={styles.btnContainer}>
                    <FlatButton text='Ajouter une réunion' onPress={() => this.addReunion()} />
                    </View>
                    <View style={styles.btnContainer}>
                    <FlatButton text='Liste des réunions' onPress={() => this.showReunions()} />
                    </View>
                </View>
                </View>

        )
    }
}

const styles = StyleSheet.create({
    content: {
        flex: 1,
        backgroundColor: '#36485f'
    },
    container: {
        flex:1,
        flexDirection: 'column',
        alignItems: 'center',
        marginTop: '60%',
    },
    imgBackground: {
        width: '100%',
        height: '100%',
        flex: 1,

    },
    btnContainer: {
        marginTop: 50,
        alignItems: 'center'
    }
})

export default HomeScreen