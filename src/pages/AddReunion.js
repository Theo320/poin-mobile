import React from 'react'
import { View, Text, BackHandler, StyleSheet, Button, Image, Picker, Modal, TouchableOpacity, Animated, AppRegistry, ScrollView } from "react-native";
import { TextInput } from 'react-native-gesture-handler';
import PickerSelect from 'react-native-picker-select';
import DatePicker from 'react-native-datepicker';



class AddReunion extends React.Component {
    constructor(props){
        super(props);

        this.state = {
            name: "",
            type: "",
            pickerDisplayed: false,
            buttonDisplayed: false,
            selectedItems: [],
            valueArray: [],
            disabled: false,
            date: "",
        }
        this.index = 0;
        this.animatedValue = new Animated.Value(0);
    }

    static navigationOptions={
        title:'AddReunion',
        headerLeft:null
    }
      

    componentDidMount(){
        
    }

    addMore = () => {
        this.animatedValue.setValue(0);
        let newlyAddedValue = {index: this.index}
        this.setState({disabled: true, valueArray: [...this.state.valueArray, newlyAddedValue]}, () => {
            Animated.timing(
                this.animatedValue,
                {
                    toValue: 1,
                    duration: 500,
                    useNativeDriver: true
                }
            ).start(() => {
                this.index = this.index + 1;
                this.setState({disabled: false})
            });
        });
    }

    setPickerValue(newValue) {
        this.setState({
          type: newValue
        })
    
        this.togglePicker();
      }
    
    togglePicker() {
        this.setState({
          pickerDisplayed: !this.state.pickerDisplayed,
          buttonDisplayed: !this.state.buttonDisplayed
        })
    }

    createReunion(){
        console.log(this.state.selectedItems);
    }

    onSelectedItemsChange = selectedItems => {
        this.setState({ selectedItems});
    };
    


    render(){
        const animationValue = this.animatedValue.interpolate(
            {
                inputRange: [0, 1],
                outputRange: [-59, 0]
            }
        );

        let newArray = this.state.valueArray.map((item, key) => {
            if((key) == this.index){
                return (
                    <Animated.View key={key} style={[styles.viewHolder, {opacity: this.animatedValue, transform: [{translateY: animationValue}]}]}>
                               <PickerSelect style={pickerStyle} placeholder={{label: 'Choisissez une activité', color: '#fff'}} onValueChange={(value) => {
                    this.setState({
                        selectedItems: [...this.state.selectedItems, value]
                    })
                }}
                items={[
                    {label: "FeedBack", value: "FeedBack"},
                    {label: "Evolution", value: "Evolution"},
                    {label: "Post-it", value: "Post-it"},
                    {label: "Group By", value: "Group By"},
                    {label: "Brain Post-it", value: "Brain Post-it"},
                    {label: "Clear repartition", value: "Clear repartition"},
                    {label: "Problem-solution", value: "Problem-solution"},

                    
                ]}
                />
                    </Animated.View>
                );
            }else{
                return (
                    <View key={key} style={styles.viewHolder}>
                      <PickerSelect style={pickerStyle} placeholder={{label: 'Choisissez une activité', color: '#fff'}} onValueChange={(value) => {
                    this.setState({
                        selectedItems: [...this.state.selectedItems, value]
                    })
                }}
                items={[
                    {label: "FeedBack", value: "FeedBack"},
                    {label: "Idées partagées", value: "ideespartagees"}
                ]} 
                />
                    </View>
                  );
            }
        });
        return (
            <View style={styles.container}>
                <Text style={styles.header}>Création d'une réunion</Text>

                <TextInput style={styles.textinput} placeholder="Nom de la réunion" placeholderTextColor="#fff" underlineColorAndroid={'transparent'} onChangeText={(text) => this.setState({name: text})} />
                
                    <PickerSelect style={pickerStyle} placeholder={{label: 'Choisissez un type de réunion', color: '#fff'}} onValueChange={(value) => {
                    this.setState({
                        type: value,
                    })
                }}
                items={[
                    {label: "Réunion de revue", value: "Reunion de revue"},
                    {label: "Réunion d'échange", value: "Réunion d'échange"},
                    {label: "Réunion de résolution de problèmes", value: "Réunion de résolution de problèmes"},
                    {label: "Brainstorming", value: "Brainstorming"},
                    {label: "Réunion de prise de décision", value: "Réunion de prise de décision"},
                ]} value={this.state.type}
                />

                <DatePicker style={{width:200, top: 20}}
                date={this.state.date}
                mode="date"
                placeholder="Date de réunion"
                format="YYYY-MM-DD"
                minDate="2020-04-10"
                maxDate="2030-12-30"
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                customStyles={{
                    dateIcon: {
                        position: 'absolute',
                        left: 0,
                        top: 4,
                        marginLeft: 0,
                    },
                    dateInput: {
                        marginLeft: 36,
                        color: '#fff'
                    },
                    dateText: {
                        color: '#fff'
                    }
                }}
                onDateChange={(date) => {this.setState({date: date})}} />


                {/*******************************************ACTIVITES****************************************************** */}


                <View style={{paddingTop: 60}}>
                <PickerSelect style={pickerStyle} placeholder={{label: 'Choisissez une activité', color: '#fff'}} onValueChange={(value) => {
                    this.setState({
                        selectedItems: [...this.state.selectedItems, value]
                    })
                }}
                items={[
                    {label: "FeedBack", value: "FeedBack"},
                    {label: "Evolution", value: "Evolution"},
                    {label: "Post-it", value: "Post-it"},
                    {label: "Group By", value: "Group By"},
                    {label: "Brain Post-it", value: "Brain Post-it"},
                    {label: "Clear repartition", value: "Clear repartition"},
                    {label: "Problem-solution", value: "Problem-solution"},
                ]}
                />

                <View style={styles.container2}>
                <ScrollView>
                    <View style={{ flex: 1, padding: 4 }}>
                        {
                            newArray
                        }
                     </View>
                </ScrollView>

                <TouchableOpacity activeOpacity={0.8} style={styles.buttonDesign} disabled={this.state.disabled} onPress={this.addMore}>
                <Image source={require('../resources/addButton.png')} style={styles.buttonImage} />
            </TouchableOpacity>
                </View>

                </View>

                <TouchableOpacity
                style={styles.saveButton}
                onPress={() => {this.createReunion()}}
                >
                <Text style={styles.saveButtonText}>Save</Text>
                </TouchableOpacity>
            </View>
             
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: '#36485f',
        paddingLeft: 60,
        paddingRight: 60,
    },
    regform: {
        alignSelf: 'stretch',
    },
    header: {
        fontSize: 24,
        color: '#fff',
        paddingBottom: 10,
        marginBottom: 40,
        borderBottomColor: '#199187',
        borderBottomWidth: 1,
    },
    textinput: {
        alignSelf: 'stretch',
        height: 40,
        marginBottom: 30,
        color: '#fff',
        borderBottomColor: '#f8f8f8',
        borderBottomWidth: 1,
    },
    picker: {
        width: null,
        fontSize: 16,
        paddingTop: 10,
        paddingBottom: 10,
        borderBottomColor: '#fff',
        borderBottomWidth: 1,
        backgroundColor: 'white',
        color: '#fff',
    },
    rowContainer: {
        flexDirection: 'column',
    },
    viewHolder: {
        height: 55,
        backgroundColor: '#36485f',
        justifyContent: 'center',
        alignItems: 'center',
        margin: 0
      },
      headerText: {
        color: 'white',
        fontSize: 25
      },
      buttonDesign: {
        position: 'absolute',
        right: -50,
        bottom: -10,
        borderRadius: 30,
        width: 40,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
      },
      buttonImage: {
        resizeMode: 'contain',
        width: '100%',
      },
      saveButton: {
        borderWidth: 1,
        borderColor: '#007BFF',
        backgroundColor: '#007BFF',
        padding: 15,
        margin: 5,
        top: 50
      },
      saveButtonText: {
        color: '#FFFFFF',
        fontSize: 20,
        textAlign: 'center'
      }
      
})

const pickerStyle = {
	inputIOS: {
		color: 'white',
		paddingTop: 13,
		paddingHorizontal: 0,
        paddingBottom: 12,
        borderBottomColor: '#fff',
        borderBottomWidth: 1
	},
	inputAndroid: {
		color: 'white',
	},
	placeholderColor: 'white',
	underline: { borderTopWidth: 1, color: '#fff' },
	icon: {
		position: 'absolute',
		backgroundColor: 'transparent',
		borderTopWidth: 5,
		borderTopColor: '#00000099',
		borderRightWidth: 5,
		borderRightColor: 'transparent',
		borderLeftWidth: 5,
		borderLeftColor: 'transparent',
		width: 0,
		height: 0,
		top: 20,
		right: 15,
	},
};

export default AddReunion