import React from 'react';
import { StyleSheet, Text, View, FlatList, SafeAreaView } from 'react-native';
import Unorderedlist from 'react-native-unordered-list';

class ShowReunion extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            reunions: [],
        }
    }


    renderItem = ({ item }) => {
        let date = new Date(item.date);
        let fullDate = date.getDate() + "/" + (date.getMonth()+1) + "/" + date.getFullYear(); 
        return(
                <View style={styles.main_container}>
        <View style={styles.content_container}>
          <View style={styles.header_container}>
            <Text style={styles.title_text}>{item.name}</Text>
        <Text style={styles.vote_text}>{fullDate}</Text>
          </View>
          <View style={styles.date_container}>
            <Text style={styles.date_text}>Type : {item.type}</Text>
          </View>
          <View style={styles.description_container}>
            <Text style={styles.description_text}>Liste des activités : </Text>
            <View style={styles.list_container}>
            {item.activites.map((value,index) => {
                return <Unorderedlist><Text>{value.name}</Text></Unorderedlist>
            })}
            </View>
          </View>
          
        </View>
      </View>
            
        );
        
    }

    componentDidMount() {
        const url = 'http://192.168.0.30:8081/api/reunions';

        fetch(url)
        .then((response) => response.json())
        .then((responseJson) => {
            this.setState({
                reunions: responseJson
            })
            console.log(responseJson);
        })
    }


    render(){
        return (
           <View style={{marginTop: 20}}>
                <Text style={{textAlign: 'center', fontSize: 32}}>Liste des réunions</Text>

                <FlatList 
                    data={this.state.reunions}
                    renderItem={this.renderItem}
                    ItemSeparatorComponent = {this.FlatListItemSeparator}
                />
           </View>
               
            
            
        );
    }

};

const styles = StyleSheet.create({
    main_container: {
      height: 190,
      flexDirection: 'row',
      marginTop: 30,
      paddingLeft: 20,
      paddingRight: 20,
      
    
    },
    content_container: {
      flex: 1,
      margin: 5
    },
    header_container: {
      flex: 3,
      flexDirection: 'row'
    },
    title_text: {
      fontWeight: 'bold',
      fontSize: 20,
      flex: 1,
      flexWrap: 'wrap',
      paddingRight: 5
    },
    vote_text: {
      fontWeight: 'bold',
      fontSize: 26,
      color: '#666666'
    },
    description_container: {
      flex: 7,
      top:10
    },
    description_text: {
      fontWeight: 'bold',
      color: '#666666',
      fontSize: 20
    },
    date_container: {
      flex: 1
    },
    date_text: {
      textAlign: 'left',
      fontSize: 14
    },
    list_container: {
        left: 30
    }
  })

export default ShowReunion 