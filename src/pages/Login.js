import React from 'react'
import {StyleSheet,Text,View, TextInput, TouchableOpacity, StatusBar} from 'react-native'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

class Login extends React.Component {

    constructor(props){
        super(props);
        this.state = ({
            pseudo:'',
            password:''
        })
    }

    updateValue(text, field){
        switch (field) {
            case 'pseudo':
                this.setState({
                    pseudo:text
                })
                break;

            case 'password':
                this.setState({
                    password: text
                })
                break;
    
            default:
                break;
        }
    }

     login(){
        var url = 'http://192.168.0.30:8081/api/login?pseudo='+this.state.pseudo;

        fetch(url,{method:'POST'}).then(function(response) {
            return response.text();
        }).then((response) => {
            if(response == 'true'){
                this.props.navigation.navigate("HomeScreen");
            }else{
                console.log("test");
            }
        });

    }

    render(){
        return(
            <View style={styles.container}>
                <StatusBar
                    backgroundColor="#1e90ff"
                    barStyle="light-content"
                />
                <Text style={styles.welcome}>Page d'authentification</Text>
                <TextInput
                    style={styles.input}
                    placeholder="Username"
                    onChangeText={(text) => this.updateValue(text, 'pseudo')}
                />
                <TextInput
                    style={styles.input}
                    placeholder="Password"
                    onChangeText={(text) => this.updateValue(text, 'password')}
                    secureTextEntry
                />
                <View style={styles.btnContainer}>
                    <TouchableOpacity 
                    onPress={() => this.login()}
                    style={styles.userBtn}>
                        <Text style={styles.btnTxt}>Login</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#36485f'
    },
    welcome: {
        fontSize: 30,
        textAlign: 'center',
        margin: 10,
        color: "#fff"
    },
    input:{
        width: "90%",
        backgroundColor: "#fff",
        padding: 15,
        marginBottom: 10
    },
    btnContainer:{
        flexDirection: "row",
        justifyContent: "center",
        width: "90%",
    },
    userBtn:{
        backgroundColor:"#FFD700",
        padding: 15,
        width: "45%",
        borderRadius: 10
    },
    btnTxt:{
        fontSize: 18,
        textAlign: 'center'
    }
})


export default Login